export const orderStatus = {
	0: '已支付',
	1: '未支付',
	2: '已退款',
	3: '已完成',
	4: '制作配送中',
	5: '已取消',
	6: '退款中',
}