
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import {myRequest} from "util/api.js"
import Util from "./util/util.js"

import '@/main.scss'

import 'uview-ui/theme.scss';
import '@/uni_modules/uni-scss/variables.scss';
import '@/main.scss';

import uView from "uview-ui";
Vue.use(uView);
Vue.prototype.$myRequest=myRequest
Vue.prototype.$util=Util
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif