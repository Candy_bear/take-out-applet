import { hideError } from './index.js';
export const loadAuth = () => {
	wx.getSetting({
	  success(res) {
	    if (!res.authSetting['scope.userInfo']) {
	      wx.authorize({
	        scope: 'scope.userInfo',
	        success () {
	          wx.startRecord()
	        }
	      })
	    }
	  }
	})
}
export const loadUserInfo = () => {
	return new Promise(resolve => {
		wx.getUserProfile({
			desc: '用户信息将用于展示',
			success({ userInfo }) {
				uni.setStorageSync('userInfo', userInfo)
				resolve(true)
			},
			error() {
				console.log(1)
				resolve(false)
			}
		})
	})
}

export const loginWX = () => {
	return new Promise(resolve => {
		wx.login({
			success({code}) {
				uni.setStorageSync('openid', code)
				resolve(true)
			},
			error() {
				resolve(false)
			}
		})
	})
}

export const getUserInfo = () => {
	return hideError(() => uni.getStorageSync('userInfo'))
}

export const getOpenId = () => {
	return hideError(() => uni.getStorageSync('openid'))
}