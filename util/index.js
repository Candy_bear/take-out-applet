export const hideError = async callback => {
	try {
		return await callback()
	} catch {
		return false
	}
}
export const hideErrorSync = callback => {
	try {
		return callback()
	} catch {
		return false
	}
}