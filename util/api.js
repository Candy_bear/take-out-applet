// const basue_URL = "http://www.inforte.cn/"
const basue_URL = "https://www.inforte.cn/api/"
export const myRequest = (options) => {
	if (options.data == undefined) {
		options.data = {}
	}
	options.data.app_type = "weapp"
	options.data.app_type_name = '微信小程序'
	uni.showLoading()
	return new Promise((reslove, reject) => {
		uni.request({
			url: basue_URL + options.url,
			data: options.data || {},
			method: options.method || 'GET',
			header: {
				"content-type": "application/x-www-form-urlencoded;",
			},
			success: (res) => {
				uni.hideLoading()
				reslove(res.data)
			},
			fail: (err) => {
				uni.hideLoading()
				reject(err)
			}
		})
	})
}
